import { useRouter } from "next/router";
import axios from "axios";
import { useState, useEffect, useMemo } from "react";
import { data } from "autoprefixer";

const FileList = () => {
  const [modules, setModules] = useState([]);
  const [fav_list, setFavList] = useState([]);
  const [votes_list, setVotesList] = useState([]);

  const router = useRouter();

  const getConsultants = () => {
    var config = {
      method: "get",
      url: `https://api.thecatapi.com/v1/images?limit=100`,
      headers: {
        "x-api-key": "6d83bbb9-bc31-4eb8-ab3d-0431eb88b460",
      },
    };
    axios(config).then((resp) => {
      console.log(resp);
      setModules(resp.data);
    });
  };
  useEffect(() => {
    getConsultants();
    getUserFav();
    getAllVotes();
  }, []);



  const addToFav = (data) => {
    console.log("data", data);
    let url = "https://api.thecatapi.com/v1/favourites";
    var config = {
      method: "post",
      url,
      data,
      headers: {
        "x-api-key": "6d83bbb9-bc31-4eb8-ab3d-0431eb88b460",
        "Content-Type": "application/json",
      },
    };
    axios(config)
      .then((resp) => {
        console.log("Add to fav: ", resp);
        getUserFav();
      })
      .catch((err) => Error("errro while adding to fav"));
  };

  const removeFromFav = (id) => {
    console.log("id", id);
    let url = `https://api.thecatapi.com/v1/favourites/${id}`;
    var config = {
      method: "delete",
      url,
      headers: {
        "x-api-key": "6d83bbb9-bc31-4eb8-ab3d-0431eb88b460",
      },
    };
    axios(config)
      .then((resp) => {
        console.log("Delete from fav: ", resp);
        getUserFav();
      })
      .catch((err) => Error("errro while deleting from fav"));
  };

  const VoteUp = (data) => {
    console.log("data", data);
    let url = "https://api.thecatapi.com/v1/votes";
    var config = {
      method: "post",
      url,
      data,
      headers: {
        "x-api-key": "6d83bbb9-bc31-4eb8-ab3d-0431eb88b460",
        "Content-Type": "application/json",
      },
    };
    axios(config)
      .then((resp) => {
        console.log("VoteUp : ", resp);
        getAllVotes();
      })
      .catch((err) => Error("errro while adding to fav"));
  };

  const voteDown = (id) => {
    console.log("id", id);
    let url = `https://api.thecatapi.com/v1/votes/${id}`;
    var config = {
      method: "delete",
      url,
      headers: {
        "x-api-key": "6d83bbb9-bc31-4eb8-ab3d-0431eb88b460",
      },
    };
    axios(config)
      .then((resp) => {
        console.log("voteDown : ", resp);
        getAllVotes();
      })
      .catch((err) => Error("errro while deleting from fav"));
  };

  const getUserFav = () => {
    let url = "https://api.thecatapi.com/v1/favourites?limit=100";
    var config = {
      method: "get",
      url,
      headers: {
        "x-api-key": "6d83bbb9-bc31-4eb8-ab3d-0431eb88b460",
      },
    };
    axios(config).then((resp) => {
      console.log("Get User fav:", resp);
      setFavList(resp?.data);
    });
  };

  const getAllVotes = () => {
    let url = "https://api.thecatapi.com/v1/votes?limit=100";
    var config = {
      method: "get",
      url,
      headers: {
        "x-api-key": "6d83bbb9-bc31-4eb8-ab3d-0431eb88b460",
      },
    };
    axios(config).then((resp) => {
      console.log("Get Votes list:", resp);
      setVotesList(resp?.data);
    });
  };

  const checkIsToggled = (list_to_check, id) => {
    let found = list_to_check.findIndex((i) => i.image_id == id);
    if (~found) {
      return { check: true, toggle_id: list_to_check[found].id };
    }
    return { check: false, toggle_id: found.id };
  };

  const getNoOfVotes = (list_to_check, id) => {
    let found = list_to_check.findIndex((i) => i.image_id == id);
    if (~found) {
      return list_to_check[found].value;
    }
    return 0;
  };

  return (
    <>
      <div className="tw-full tw-bg-white tw-shadow-md  tw-p-5 tw-mb-5 tw-mt-5">
        <div className="tw-grid tw-w-full md:tw-grid-cols-2 lg:tw-grid-cols-4  tw-gap-8 items-center justify-center ">
        
        {modules.map((data) => (
          <div key={data.id} className="tw-w-full md:tw-w-60 tw-text-center">
            <div className="catImg tw-shadow-lg  tw-h-36 tw-w-full tw-rounded-lg tw-mb-5">
              <img src={data.url} width="100px" title={data.original_filename} alt="" className="tw-h-full tw-m-auto tw-object-cover tw-rounded-lg tw-w-full"/>
            </div>
            <div className="catdetails tw-w-full">
            {/* <span>{data.id}</span> */}
            {checkIsToggled(fav_list, data.id).check ? (
              <span
                className="tw-inline-block tw-w-16 "
                onClick={() =>
                  removeFromFav(checkIsToggled(fav_list, data.id).toggle_id)
                }
              >
                ❤
              </span>
            ) : (
              <span className="tw-inline-block tw-w-16"
                onClick={() =>
                  addToFav({
                    image_id: data.id,
                    sub_id: data.sub_id || "your-user-1234",
                  })
                }
              >
                🤍
              </span>
            )}
            
            {checkIsToggled(votes_list, data.id).check ? (
              <span className="tw-inline-block tw-w-16"
                onClick={() =>
                  voteDown(checkIsToggled(votes_list, data.id).toggle_id)
                }
              >
                👍<p className="tw-inline-block tw-w-1">({getNoOfVotes(votes_list, data.id)})</p>
              </span>
            ) : (
              <span className="tw-inline-block tw-w-16"
                onClick={() =>
                  VoteUp({
                    image_id: data.id,
                    sub_id: data.sub_id || "my-user-1234",
                    value: 1,
                  })
                }
              >
                👎<p className="tw-inline-block tw-w-1">({getNoOfVotes(votes_list, data.id)})</p>
              </span>
            )}
            
          </div>
          </div>
        ))}
      </div>
      </div>
      

    </>
  );
};
export default FileList;
