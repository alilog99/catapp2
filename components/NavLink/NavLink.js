import React,{useContext} from 'react';

import Link from 'next/link'
import { useRouter } from 'next/router';


const NavLink = ({title,url,isInternalLink})=>{
    return(
    <a href={url} className="tw-px-1 tw-py-2 md:tw-px-2 md:tw-py-4 hover:tw-border-primaryColor hover:tw-text-white tw-tracking-wide tw-leading-loose tw-cursor-pointer tw-font-display tw-font-extrabold">{title}</a>
    )
}

export default NavLink;