import { useRouter } from "next/router";
import { useState } from "react";
import NavLink from "./NavLink/NavLink";
import logo from '../public/cat.png';

const Header = () => {
return (
    <div className="md:tw-flex md:tw-flex-col tw-bg-gradient-to-b tw-from-red-600 tw-via-red-300  tw-to-transparent tw-w-full tw-px-0 tw-justify-center tw-items-center tw-py-1 lg:tw-flex-row">
      <div className="md:tw-flex tw-flex-col  tw-w-full tw-max-w-6xl tw-px-0 md:tw-justify-start tw-items-center tw-py-2 lg:tw-flex-row">
        <div className="md:tw-flex tw-flex-row tw-item-center  md:tw-justify-start tw-ml-4 tw-w-full lg:tw-w-3/12 text-lg tw-text-black tw-font-extrabold">

            <svg version="1.0" xmlns="http://www.w3.org/2000/svg"
            width="228.000000pt" height="375.000000pt" viewBox="0 0 228.000000 375.000000"
            preserveAspectRatio="xMidYMid meet" className="tw-h-16 tw-w-16">

            <g transform="translate(0.000000,375.000000) scale(0.100000,-0.100000)"
            fill="#000000" stroke="none">
            <path d="M769 3712 c-86 -56 -198 -194 -289 -357 l-34 -60 95 95 c78 79 173
            157 282 232 16 11 17 21 11 66 -3 28 -10 52 -13 52 -4 0 -27 -13 -52 -28z"/>
            <path d="M949 3642 c-123 -45 -316 -192 -439 -335 -243 -282 -411 -646 -481
            -1043 -30 -169 -33 -476 -6 -624 45 -243 152 -469 288 -608 30 -31 48 -57 43
            -62 -5 -5 -83 -38 -174 -74 l-165 -66 900 0 c559 0 913 -4 935 -10 73 -20 127
            -110 111 -182 -21 -91 -70 -121 -208 -126 l-102 -4 -3 78 -3 79 -395 2 -395 1
            138 -26 137 -26 0 -53 0 -53 -339 0 c-328 0 -339 -1 -365 -21 -56 -44 -70
            -110 -36 -166 37 -62 44 -63 410 -63 l330 0 0 -130 0 -129 98 61 c53 33 112
            70 129 80 l33 20 87 -55 c124 -77 159 -97 167 -97 4 0 6 56 4 125 l-3 125 157
            0 c189 0 237 14 322 91 131 119 181 270 141 424 -18 67 -44 113 -97 171 l-38
            41 29 54 c60 111 76 181 76 329 0 126 -2 141 -32 223 -90 253 -276 442 -514
            521 -114 38 -233 55 -452 65 -220 10 -251 18 -245 69 4 35 50 60 222 122 172
            61 261 105 313 156 65 61 61 82 -33 171 l-76 72 -9 118 c-9 135 -32 220 -77
            285 -55 81 -176 138 -289 138 -44 0 -53 3 -53 18 0 16 44 262 55 310 5 19 1
            22 -22 21 -16 0 -49 -8 -74 -17z"/>
            </g>
            </svg>
            Cat API

          
          
        </div>
        
        <div className={` tw-items-center tw-justify-center lg:tw-flex tw-flex-row lg:tw-items-end lg:tw-justify-end tw-mr-0 tw-w-9/12 `}>
          <nav
            className={`tw-nav tw-self-center tw-px-0 `}
          >
            <ul className="tw-flex tw-flex-col tw-justify-items tw-items-center lg:tw-flex-row text-lg tw-text-black  tw-font-extrabold">
              
              <NavLink
                title="Cat List"
                isInternalLink={true}
                url="/"
              ></NavLink>
              <NavLink
                title="Upload"
                isInternalLink={true}
                url="/upload"
              ></NavLink>
              {/* <NavLink title="Profile" isInternalLink={true} url="/profile-information"></NavLink> */}
              
            </ul>
          </nav>
        </div>


        
      
      </div>
    </div>
  );
};

export default Header;