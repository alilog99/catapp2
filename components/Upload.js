import axios from "axios";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";

const Upload = () => {
  const [file, setFile] = useState(null);
  const [isLoading ,setIsLoading] = useState(false)
  const router = useRouter();

  const onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
       console.log("name:", event.target.files[0].name);
      let img = event.target.files[0];
      setFile(img)
      // onUploadImagePressed(img, img.name);
      //   setState({
      //     image: URL.createObjectURL(img),
      //   });
    }
  };

  console.log("file",file)
  const onUploadImagePressed = () => {
    setIsLoading(true)
    var formData = new FormData();
    formData.append("name", file?.name);
    formData.append("file", file);
    console.log("form",formData)
    var config = {
      method: "post",
      data: formData,
      url: `https://api.thecatapi.com/v1/images/upload`,
      headers: {
        "x-api-key": "6d83bbb9-bc31-4eb8-ab3d-0431eb88b460",
        "Content-Type": "multipart/form-data",
      },
    };
    axios(config)
      .then((resp) => {
        console.log("resp", resp);
        if (resp.status == "201" || resp.status == "200") {
          setIsLoading(false)
          router.push("/");
          setFile(null)
       
        } else {
          setIsLoading(false)
          Error("Error uploading image");
        }
      })
      .catch((err) => {
        setIsLoading(false)
        console.log(err)
        Error("Error uploading image");
      });
  };

  return (
    <div className=" tw-full tw-bg-white tw-px-2 tw-mt-10 tw-mb-10 tw-p-5 tw-rounded-lg tw-shadow-lg">
    <div className=" tw-max-w-md tw-mx-auto tw-bg-white tw-rounded-lg tw-overflow-hidden md:tw-max-w-lg">
        <div className="md:tw-flex">
            <div className="tw-w-full">
                <div className="tw-p-4 tw-border-b-2"> <span className="tw-text-lg tw-font-bold tw-text-gray-600">Add Image</span> </div>
                <div className="tw-p-3">
                    <div className="tw-mb-2"> <span>Attachment</span>
                        <div className="tw-relative tw-h-40 tw-rounded-lg tw-border-dashed tw-border-2 tw-border-gray-200 tw-bg-white tw-flex tw-justify-center tw-items-center hover:tw-cursor-pointer">
                            <div className="tw-absolute">
                                <div className="tw-flex tw-flex-col tw-items-center "> <i className="tw-fa tw-fa-cloud-upload tw-fa-3x tw-text-gray-200"></i> <span className="tw-block tw-text-gray-400 tw-font-normal">Attach you files here</span> <span className="tw-block tw-text-gray-400 tw-font-normal">or</span> <span className="tw-block tw-text-blue-400 tw-font-normal">{(file === null) ? "Browse files" : file?.name}</span> </div>
                            </div> <input type="file" onChange={onImageChange} className="tw-h-full tw-w-full tw-opacity-0" name="" />
                        </div>
                        <div className="tw-flex tw-justify-between tw-items-center tw-text-gray-400"> <span>Accepted file type: Cat Image</span> <span className="tw-flex tw-items-center "><i className="fa fa-lock tw-mr-1"></i> secure</span> </div>
                    </div>
                    <div className="tw-mt-3 tw-text-center tw-pb-3"> {!isLoading ?<button onClick={onUploadImagePressed}className="tw-w-full tw-h-12 tw-text-lg tw-bg-blue-600 tw-rounded tw-text-white hover:bg-blue-700">Upload Image</button> : <button type="button" className="tw-inline-flex tw-items-center tw-px-4 tw-py-2 tw-border tw-border-transparent tw-text-base tw-leading-6 tw-font-medium tw-rounded-md tw-text-white tw-bg-blue-600 hover:tw-text-blue-400 focus:tw-border-blue-700 active:tw-bg-rose-700 tw-transition tw-ease-in-out tw-duration-150 tw-cursor-not-allowed" disabled="">
        <svg className="tw-animate-spin tw--ml-1 tw-mr-3 tw-h-5 tw-w-5 tw-text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
          <circle className="tw-opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4"></circle>
          <path className="tw-opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
        </svg>
        Processing
      </button>}</div>
                </div>
            </div>
        </div>
    </div>
</div>
    
    
     
    
  );
};

export default Upload;
