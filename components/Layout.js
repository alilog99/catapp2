import Head from 'next/head';

import Header from './Header';
import Footer from './Footer';

const Layout = props => {
  return (
    <>
     
      <div>
        <div >
          <Header />
          <div className="tw-container-fluid mx-auto p-5 tw-flex tw-flex-col tw-w-full tw-bg-backgroundLight tw-items-center tw-justify-center tw-m-h-full">{props.children}</div>
        </div>
        <Footer />
      </div>
    </>
  );
};

export default Layout;
