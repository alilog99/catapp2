const colors = require("tailwindcss/colors");
const plugin = require("tailwindcss/plugin");
const defaultTheme = require('tailwindcss/defaultTheme')

/*
  Creates colored underline utility classes.

  Explanation: https://css-tricks.com/almanac/properties/t/text-decoration-color/
  Browser Support: https://caniuse.com/#search=text-decoration-color

  e.g

  .underline-black => text-decoration: underline #000 !important;
  .underline-green-400 => text-decoration: underline #68d391 !important;
*/
const underlineColorPlugin = plugin(function ({
  addUtilities,
  e,
  theme,
  variants,
}) {
  const colors = theme("colors", {});
  const underlineColorVariants = variants("underlineColor", []);

  const utilities = Object.entries(colors).map(([color, colorValue], i) => {
    if (typeof colorValue === "object") {
      return Object.entries(colorValue).map(([colorVariant, value]) => {
        return {
          [`.underline-${e(color)}-${e(colorVariant)}`]: {
            textDecoration: `underline ${value}`,
          },
        };
      });
    } else {
      return {
        [`.underline-${e(color)}`]: {
          textDecoration: `underline ${colorValue}`,
        },
      };
    }
  });

  addUtilities(utilities, underlineColorVariants);
});

module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}","./uicomponents/**/*.{js,ts,jsx,tsx}"],

  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      'portevo-body':'#676767',
      primaryColor: "#D63418",
      secondaryColor: "#f2f2f2",
      transparent: "transparent",
      primaryBlue: "#000586",
      h6FontColor:'#231c24',
      imageBackground:'#d7d7d7',
      backgroundLight: "#F7F7F7",
      backgroundDark: "#F2F2F1",
      homeTileBg: "#F2F2F2",
      current: "currentColor",
      whiteColor: "#ffffff",
      stepperDisableColor: "#848484",
      stepperDisabledDescColor: "#A9AAAA",
      black: colors.black,
      white: colors.white,
      gray: colors.coolGray,
      red: colors.red,
      yellow: colors.amber,
      blue: colors.blue,
      tileSelectedBG:'#ED5735'
    },
    maxHeight: {
      500: "500",
    },

   

    fontFamily: { 
      'custom': ["Proxima Nova",...defaultTheme.fontFamily.sans], 
    },
    extend:{
      lineHeight:{
        'line-height-header':'40px',
        'line-height-menu':'26px',
        'line-height-sub-header':'30px',
        'line-button':'14px'
        
      },
      fontSize:{
        'base2':'32px',
        'base1':'17px',
        'button':'14px'
      },
      borderRadius:{
        '4':'4px'
      },
      spacing:{
        'button-18':'18px',
        'button-51':'51px'
      },
      width:{
        '26':'6.5rem'
      }
      
    }
  },

  prefix: "tw-",
  variants: {
    extend: {
      padding: ["hover"],
    },
    underlineColor: ["hover"],
  },
  plugins: [underlineColorPlugin],
};
